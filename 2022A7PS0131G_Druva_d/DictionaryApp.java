class DictionaryApp {
  // Provides only a main method for instantiating and demonstrating a dictionary
  public static void main(String args[]) {
    int size;
    int count;
    size=Integer.parseInt(args[0]);
    count=Integer.parseInt(args[1]);
    System.out.println("Reading " + count + " elements into a dictionary of size " + size);
    MyDictionary d=new MyDictionary(size);
    String[] elems=java.util.Arrays.copyOfRange(args, 2, args.length);
    for(int i=0; i<count; i++) {
      d.put(new Record(Double.parseDouble(elems[i])));
    }
    d.records();
    d.keys();
    System.out.println("Enter key of record you want");
    int key = Integer.parseInt(System.console().readLine());
    System.out.println("Value: " + d.get(key));
    System.out.println("Enter key of record you want to remove");
    key = Integer.parseInt(System.console().readLine());
    System.out.println("Removed: " + d.remove(key).getValue());

  }
}
